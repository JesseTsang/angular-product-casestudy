# OrganicWorld

## Problem Statement

Organic World have started business in 2013 as a organic food store. It started in a 100 sqft store at Detroit. Because of high quality products, resonable price and steady supply the store became popular, currently operates with twenty stores in Detroit and planning to open multiple stores across the country.

Organic World wants to digitize their operation. The objective of Organic world is to create a brand of itself and increase their customer volume.

## Proposed Solution

In order to increase their product consumers Organic world needs a solution that will help to reach out to users over the Web for buying their products.

## The roles in this application are

- Customer: Request for a product available in store and buy directly through online transaction

## Process To be implemented in Phase 1


## Backend

Product.json  create in a server folder and run using json-server


Model of Product.json
	
Id : number

Productname :string

Qty : number

## Front end

** Use the following Components

•	Home

•	Login

•	Dashboard

•	Header

•	Footer


#### Header and Footer to be displayed in all component

#### Build routing for login, dashboard, Home. Make Default component as Home



** create the below Services

•	Customerorder – for accessing HttpClient
Method :
            addProduct(data : Product)

            fetchProduct()
          

•	Organicroute  -- for navigating to components
Methods: 
            openLogin(), 
            openDashboard(), 
            openHome()


** Protect dashboard with canactivate guard

** create a product class


Detailed view of components

### Header component 

•	should consist of mat tool bar with heading to be “Organic World”

•	Should have router link for home, login



### Footer component should have, Contact Address and phone number



### Login component

	Should have username and password text input along with sign in button


When you sign in , it needs to authenticated in canactivate guard, 

If username is admin and password in password then navigate to dashboard


### Dashboard component

Should have input text boxes for product and when the user  submit the data to be stored in product.json. 

Fetch data from product.json and Display the  details using mat-card in dashboard 


Provide an option for logout


•	Use HttpClient in  Customerorder  service
      use getProduct() method for getting the data
      











