import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/Product';

@Injectable({
  providedIn: 'root'
})

export class CustomerorderService {
  private _url: string = "http://localhost:3000/products";
  private id;

  constructor(private http: HttpClient) { }

  /**
   * GET all inventory data
   * 
   * @returns 
   */
  fetchProduct() : Observable<any> {
    let result = this.http.get(this._url);
    
    let dataPack = result.subscribe(data => {
      this.id = Object.keys(data).length;
    });

    return result;
  }
  
  /**
   * 
   * @param data 
   */
  addProduct(data : Product): Observable<any> {
    this.fetchProduct(); // stupid hack to get around the id problem now

    let payload =  {
      "id": this.id + 1,
      "productName": data.productName,
      "qty": data.qty,
      
    };

    let payloadJSON = JSON.stringify(payload);

    if (data != null) {
      const headers = { 'content-type': 'application/json' };

      let result = this.http.post(this._url, payloadJSON, { 'headers': headers });

      return result;
    }
    
    return null;
  }
}
