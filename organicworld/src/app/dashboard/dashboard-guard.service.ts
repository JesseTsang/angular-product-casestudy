import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { UserService } from "../services/user.service";


@Injectable()
export class DashboardGuardService implements CanActivate {
    
    constructor(private _userService: UserService, private _router: Router) {}

    /**
     * 
     * @param route 
     * @param state 
     * @returns 
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // this._userService.login()
        

        return false;
    };

}