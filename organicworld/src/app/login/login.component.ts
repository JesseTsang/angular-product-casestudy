import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  // message to be display if Issue added or not
  message = '';

  // showMessage is for validating contact is added or not
  showMessage;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  // Implement onSubmit method to save a Issue, verify form is valid or not
  // Display message 'Title and Description should not be empty!!! Please verify details' if form is invalid
  // Display message 'Failed to add Issue!!' while error handling
  // Display message 'Issue added' if Issue is added
  onSubmit() {
    if (this.form.controls['username'].value === '' || this.form.controls['password'].value === '') {
      this.message = 'Username and password should not be empty!!! Please verify details';
      this.showMessage = true;
      return;
    }

    this.clearForm();
  }

  // clearForm method is to reset the form after submitting
  clearForm() {
    this.form = new FormGroup({
      username: new FormControl(''),
      password: new FormControl('')
    });

    this.showMessage = false;
  }
}
