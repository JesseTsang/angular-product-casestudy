import { Component, OnInit } from '@angular/core';
import { Product } from '../models/Product';
import { CustomerorderService } from '../services/customerorder.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // products is to store all product data
  products: Product[] = [];

  constructor(private customerOrderService: CustomerorderService) { }

  ngOnInit(): void {
    this.customerOrderService.fetchProduct().subscribe(
      data => {
        this.products = data;
        console.log("From home.components.ts: " + data);
      }
    );
  }

}
