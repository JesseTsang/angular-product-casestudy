// Data model for Product
export class Product {
    id? : number;
    productName? :string;
    qty? : number;
}

